import snap

titles = []

# Load title map with id
with open('pageid_vs_title.txt') as fobj:
    for line in fobj:
        row = line.split()
        while titles.__len__()<int(row[0]):
            titles.append("")
        titles.append(row[1])

print ("done load title")
G = snap.LoadEdgeListStr(snap.PNGraph, "page_to_pages.txt", 0, 1) # Load graph from file

MxScc = snap.GetMxScc(G) # tim do thi lien thong lon nhat
print(MxScc.BegNI().GetId())

PRankH = snap.TIntFltH()
snap.GetPageRank(MxScc, PRankH)
PRankH.SortByDat(Asc) # sort ranking

ranking=[]
for item in PRankH:
    ranking.append((titles[item],PRankH[item]))

ranking_file = open('1_20172_IT4868_Assignment01_Group13_ranking.txt', 'w')

ranking_file.write("Diderot_-_Encyclopedie_1ere_edition_tome_17.djvu/20  ")
ranking_file.write("%d\n" % MxScc.GetNodes())
ranking_file.write("Pagerank Title\n")

for item in ranking:
    # print(item[0])
    if item[0]!="":
        ranking_file.write("%s   " % item[0])
        ranking_file.write("%f\n" % item[1])

print("done")
